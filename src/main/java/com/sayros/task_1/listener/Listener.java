package com.sayros.task_1.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

@Component
public class Listener {

    private TargetDAO targetDAO;

    @Autowired
    public void init(TargetDAO targetDAO) {
        this.targetDAO = targetDAO;
    }

    @JmsListener(destination = "inbound.queue")
    @SendTo("outbound.queue")
    public String receiveMessage(final Message jsonMessage) {

        final String message = jsonMessage.getPayload().toString();

        TargetEntity targetEntity = new TargetEntity();
        targetEntity.setMessage(message);
        targetDAO.save(targetEntity);

        return message;
    }

}
