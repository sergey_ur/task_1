package com.sayros.task_1.listener;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TargetDAO extends CrudRepository<TargetEntity, Long> {
}
