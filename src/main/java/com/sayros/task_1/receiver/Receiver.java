package com.sayros.task_1.receiver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/in")
class Receiver {

    @Autowired
    private JmsTemplate jmsTemplate;

    @PostMapping
    public String info(@RequestParam(value = "mess") String key) {
        jmsTemplate.send("inbound.queue", s -> s.createTextMessage(key));
        return "ok";
    }

}
