$(document).ready(function () {
    $("#btn").click(
        function () {
            sendAjaxForm('result_form', 'ajax_form', 'in');
            return false;
        }
    );
});

function sendAjaxForm(result_form, ajax_form, url) {
    jQuery.ajax({
        url: url,
        type: "POST",
        dataType: "html",
        data: jQuery("#" + ajax_form).serialize(),
        success: function (response) {
            $("#inputMessage").val('');
            $("#resultSender").text("Сообщение успешно отправленно.")
        },
        error: function (response) {
            $("#resultSender").text("При отправке сообщения произошла ошибка.")
        }
    });
}